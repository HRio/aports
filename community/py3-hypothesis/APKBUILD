# Contributor: Drew DeVault <sir@cmpwn.com>
# Maintainer: Leo <thinkabit.ukim@gmail.com>
pkgname=py3-hypothesis
pkgver=4.53.3
pkgrel=0
pkgdesc="Advanced property-based (QuickCheck-like) testing for Python"
url="https://hypothesis.works/"
arch="noarch"
license="MPL-2.0"
depends="py3-attrs"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-coverage py3-tz py3-numpy py3-dateutil py3-pexpect"
source="py-hypothesis-$pkgver.tar.gz::https://github.com/HypothesisWorks/hypothesis-python/archive/hypothesis-python-$pkgver.tar.gz
	"
builddir="$srcdir/hypothesis-hypothesis-python-$pkgver/hypothesis-python"

replaces="py-hypothesis" # Backwards compatibility
provides="py-hypothesis=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	# In python3 mock is actually unittest.mock
	sed -e 's/from mock/from unittest.mock/' -i tests/cover/test_regressions.py
	sed -e 's/from mock/from unittest.mock/' -i tests/cover/test_reflection.py

	# Add variables that will be used by python to find the local
	# hypothesis build
	PYPATH="$PWD/build/lib"

	# PYTHONPATH="$PYPATH" python3 -m pytest tests/cover
	PYTHONPATH="$PYPATH" python3 -m pytest tests/py3
	PYTHONPATH="$PYPATH" python3 -m pytest tests/datetime
	PYTHONPATH="$PYPATH" python3 -m pytest tests/numpy
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="ed3cd56ff09d4d245f70d64dd5dd19985fabfb8ae604355aa45f67d2aa9d4b7931927ea8e3f64a092904d09ead205731290b5505b1d878017642cbf5ed0fc95e  py-hypothesis-4.53.3.tar.gz"
